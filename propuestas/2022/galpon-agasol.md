---
layout: 2022/post
section: proposals
category: venues
title: GALPon - AGASOL
---

*Nombre del equipo*: GALPon - AGASOL

## Equipo organizador local:

Miguel A. Bouzada, presidente de GALPon, será el contacto local con la organización general de esLibre. GALPon (Grupo de Amigos de Linux de Pontevedra) es una asociación cultural sin ánimo de lucro dedicada a promover y difundir el uso del software libre en la sociedad gallega. Su principal campo de actuación es la provincia de Pontevedra, en Galicia, España; donde han llevado eventos de gran relevancia para el software libre como es Akademy-es 2019. Además, también como coorganizadores de esta edición, contarán con el apoyo de AGASOL (Asociación de Empresas Galegas de Software Libre Agasol), una entidad con la que han venido trabajando históricamente también con la intención de mejorar la implantación de la cultura libre en los diferentes estamentos de la región.

## Formato del congreso:

El congreso seguirá el mismo formato que en años anteriores: charlas, talleres, salas de la comunidad... teniendo la posibilidad de realizarse en formato mixto gracias a los medios digitales proporcionados por Librebit, uno de los patrocinadores de esta edición del congreso.

## Acciones para mejorar la diversidad del congreso:

Además de las acciones planteadas por la organización general, las comunidades anfitrionas se harán cargo de financiar los gastos del viaje a 4 mujeres cuyas propuestas salieran seleccionadas del proceso de envío de propuestas.

## Posibles fechas:

Viernes 24 y sábado 25 de junio de 2022.

## Aspectos económicos:

Disponibilidad de financiación gracias al Convenio de Colaboración entre las Asociaciones de Usuarios de Software Libre de Galicia y la Xunta de Galicia.

## Facilidades disponibles:

Por concretar en función del emplazamiento final a utilizar para la realización del congreso.

## Planificación y uso del espacio físico:

Dependerá de si finalmente se realiza en la Universidad de Vigo, MARCO o cualquier otro emplazamiento de la región.

## Comentarios:

Ninguno.
