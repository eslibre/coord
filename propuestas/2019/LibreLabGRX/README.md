# Propuesta de Granada - UGR.

![La ETSIIT desde el párking](etsiit-ugr.jpg)

El grupo de organización incluiría
a [JJ](https://github.com/JJ), [Germán](https://github.com/germaaan/)
y posiblemente más gente ligada
a [Interferencias](https://interferencias.tech) y
la [ETSIIT](https://etsiit.ugr.es), que celebra el 25 aniversario de la
inauguración de su primera sede propia para impartir los estudios de
Informática.

## Fecha

*21 de junio de 2019*

### Fechas de propuestas.

* Apertura de la llamada de propuestas, tracks principales: *1 de marzo*.
* Apertura de la solicitud de devrooms: *1 de marzo*.
* Cierre solicitud de devrooms: *31 de marzo*.
* Cierre solicitud de propuestas y lightning talks: *15 de mayo*.
* Comunicación devrooms: *15 de abril*.
* Entrega de planificación de devrooms: *31 de mayo*.
* Comunicación propuestas: *15 de mayo*.
* Cierre de programa: *7 de junio*.


## Sede

La [ETSIIT](https://etsiit.ugr.es) tiene un salón de actos con
capacidad para unas 200 personas, salón de grados para 60, y aulas con
capacidad desde 40 hasta 100 personas; salas de reuniones con 12 o 25
personas, un hall (para mesas y pósters, si se busca eso), cafetería
en la propia Escuela, y comedor universitario que podría usarse en
caso necesario.

La
[ETSIIT está en un barrio periférico](https://www.google.es/maps?q=37.196689,+-3.624534&hl=es&ll=37.196771,-3.62455&spn=0.0035,0.008256&sll=37.196809,-3.624533&sspn=0.0035,0.008256&t=h&z=18),
pero tiene buenas comunicaciones, con una parada del "metro" a unos 10
minutos andando y paradas de autobús cerca del mismo edificio. Hay
aparcamientos en los alrededores, bares y cafeterías.

La universidad tiene habilitado un sistema para cuentas WiFi de
invitados, tanto en puerto 80/443 como puerto 22 para ssh. Dado que es
la escuela de informática, no debe tener problema para manejar un
cierto volumen de personas conectadas.

## Alojamiento

En la inmediación de la ETSIIT no hay ningún hotel, pero hay varios a
3-4 paradas de autobús. Generalmente se acuerda un precio concertado
para un número fijo de habitaciones, dependiendo de la previsión, con
unos cuantos hoteles.

Hay también todo tipo de apartamentos y alojamientos para todos los
precios, y en junio es temporada baja, así que no tendría que haber
ningún problema para encontrar uno que se ajuste a cualquier
presupuesto. 

## Formato

En principio, el formato [propuesto](../formato.md) se puede acomodar
sin ningún problema. Se repartiría así:

* Salón de actos, track principal "Cuanto hemos cambiado".
* Aulas "0", *lightning talks*. Estas aulas tienen aire acondicionado,
  y capacidad para unas 100 personas.
* Salón de Grados: track generalista adicional o lightning talks.
* Aula -1.2: Tutoriales
* Sala de juntas, sala de usos múltiples: hackathones, para unas 20
  personas cada una.
* Aulas 0: devrooms propuestas por la comunidad.

Se haría un *call for devrooms* un tiempo antes (ver fechas propuestas
arriba). Se propone para mayor transparencia que las devrooms se
soliciten mediante un PR al repositorio de coordinación, donde se
podrá comentar en abierto la PRs y dar buenas razones para rechazarla
si es que se da el caso y no tuviéramos espacio suficiente. Las PRs
incluirán reglas claras sobre qué hace falta para solicitar una
devroom, por ejemplo
* Grupo de usuarios establecido.
* Centrado en el software, hardware u otras tecnologías libres.
* Dispuesto a hacer una llamada a participación abierta.
* Aceptar un código de conducta.
* Asistir al evento y coordinarse con el resto.

Para que se rellenen todos estos puntos, se creará una plantilla de PR.

El horario sería de 10 de la mañana a 8 de la tarde, con cervezas al
final del día en el bar de la Escuela hasta que se cierre a las 10.

## Transporte

Granada está bien comunicada por autobús, muy mal por tren (aunque
para entonces puede haber llegado el AVE), y regular por avión (vuelos
diarios a Madrid y Barcelona). Se solicitarían precios especiales a
RENFE y ALSA para venir al evento.

También se pueden organizar *carpools* para grupos que vengan de la
misma ciudad y quieran ir y volver en el día, sobre todo de otras
ciudades andaluzas o de Murcia.

## Café, cátering y comidas

Se puede solicitar cátering por parte de los comedores universitarios
y hacerlo en el mismo comedor universitario, que tiene una sede en la
ETSIIT.

Sin embargo, por cantidad de gente es más razonable que se haga lo que
se ha hecho en otras ocasiones: dar cupones (*hispalinuxcoins*) para
gastárselos en los bares cercanos en café, cerveza o lo que le dé a
cada uno la gana y a la hora que le dé la gana, en plan
autoorganizado. Con unos cupones de 6€ por persona es suficiente. Si
hay patrocinio, se puede sacar de ahí; si no lo hay, simplemente que
la gente se organice.

Y ya que hablamos de patrocinios...

## Patrocinios

Se puede solicitar por parte de la ETSIIT que patrocine al menos un
café, el de la mañana, que sería en la cafetería de la ETSIIT o en el
comedor universitario (a ambos lados del salón de Actos.)

Se pueden buscar patrocinios adicionales localmente en Granada o
fuera.

En principio, el evento se pude hacer a coste cero porque la sede no
tiene coste. En función de los patrocinios que se reciban se puede
empezar a pensar en:

* Pausa café
* Comida del mediodía.
* Camisetas
* Acreditaciones
* Grabación y emisión de las salas.
* Cartelería

## Manejo de inscripciones y ponencias

Las inscripciones se pueden manejar
desde [Granada Geek](https://www.meetup.com/es-ES/Granada-Geek/), un
meetup de la comunidad que tiene ahora mismo unas 1300 personas. Usa
Meetup, que es una web privativa, pero a cambio tiene un API
abierto. Si esto no resulta aceptable para la comunidad, se puede
habilitar un sistema de inscripción basado en tecnologías libres, como
el que usa Interferencias.

Las solicitud de ponencias puede usar el mismo sistema que
las [JASYP](https://interferencias.tech/jasyp/). Se ha usado también
para otros eventos, y no va mal; es software libre y se puede
modificar como se quiera. Alternativamente, la solicitud de ponencias
se puede hacer mediante PRs a un repositorio específico, con un
"comité de programa" que revisará las ponencias públicamente.

## Equipo

El equipo organizador tiene experiencia anterior; Germán ha organizado
las JASYP en sus tres ediciones hasta ahora, y JJ ha estado en todo
tipo de conferencias, desde pequeños eventos como las últimas jornadas
de Perl hasta la conferencia europea de Perl en 2015, con más de 300
personas.

Para ayudar en cada una de las salas se organizarían voluntarios entre
los estudiantes de la ETSIIT, para los que se solicitarían créditos de
libre configuración.

## Conciliación

Para quien se tenga que venir con la familia, se habilitará una *zona
de conciliación* con personas voluntarias que organizarán actividades
para los más pequeños, para que sus madres y padres puedan asistir a
las charlas tranquilamente. Algunas posibilidades:

* Talleres de Scratch en salas de ordenadores de la Escuela.
* Actividades lúdicas en el patio.
* Actividades con *hama beads* o papiroflexia en alguna de las salas,
  por ejemplo la sala de Juntas.
* Visita guiada
  al [mini-museo de informática](https://jj.github.io/informuseo/) de
  la escuela, con actividades como aprender código binario perforando
  tarjetas
  o
  [aprender a programar bailando](https://medium.com/@jjmerelo/programemos-la-m%C3%A1quina-de-asombrar-b0a96a5709e9).

## Apoyo institucional

La ETSIIT apoyará el evento de forma protocolaria y también con un
pequeño patrocinio; podremos coordinarnos con ellos para solicitar
patrocinios adicionales. Si hay que manejar patrocinios y hacer pagos
se tiene que hacer desde una cuenta institucional, en cuyo caso hay
varias posbilidades:
* Abrir una cuenta específica para el evento.
* Usar una cuenta existente, del grupo de investigación, por ejemplo.
* Usar una cuenta existente de la ETSIIT.

Este apoyo se puede usar también para pagar el viaje y estancia para
alguna ponencia que se decida.

## Ruta turística geek el sábado

Para los que queden el sábado por la mañana, se puede organizar una
ruta alternativa, geek, por Granada

* Ruta *geek* de visita a estructuras steampunk, lugares de especial
  resonancia ingenieril o geek, y las matemáticas en los monumentos.
* Ruta de graffiti

También nos podemos ir de tapas, vamos. Eso ya a gusto de cada cual.
