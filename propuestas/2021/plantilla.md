# Propuesta para la Convocatoria de Sede esLibre 2021 de **[CIUDAD/ORGANIZACIÓN/EMPRESA]**

## Equipo organizador

El equipo organizador de esta propuesta estará compuesto por (añadir tantas personas como sean necesarias):
* **[...]**
* **[...]**
* **[...]**

### Experiencia y motivación

* **[...]**

## Formato del congreso

* [ ] Presencial o [ ] Virtual

  * Ideas para los main tracks: **[...]**
* [ ] Posibilidad de participación de otras comunidades mediante organización de salas
* [ ] Espacio para sesiones plenarias de apertura y cierre del congreso con todos los asistentes, participantes y organizadores

## Posibles fechas

  * Fechas estimadas para la realización del congreso: **[...]**
* [ ] Posibilidad de que el evento se desarrolle en fin de semana (total o parcialmente)
* [ ] Nos comprometemos en la medida de lo posible a evitar que la fecha final del congreso choque con la de otro congreso de temática similar

## Presupuesto económico

* [ ] La asistencia al evento será gratuita
* Posible financiación:
    * [ ] Empresas privadas (nos comprometemos a que no sean por parte de compañías cuya actividad pudiera ser contraria a la filosofía del evento)
    * [ ] Presupuestos/subvenciones de instituciones públicas
* Posibles beneficios para el congreso en los que se invertiría el patrocinio: **[...]**

## Facilidades disponibles

En el caso de evento presencial:

* [ ] Disponibilidad de lugar físico para el desarrollo del congreso (indicar a continuación): **[...]**
* [ ] Posibilidad de catering de algún tipo (cafés, bollería, aperitivos, tentempiés...): **[...]**
* [ ] Posibilidad de descuentos en transporte desde los diferentes puntos del país
* [ ] Posibilidad de alojamientos asequibles o descuentos cercanos a la sede del evento

En el caso de evento virtual:

* [ ] Disponibilidad de infraestructura (hardware) para soportar las soluciones software necesarias para el correcto desarrollo del congreso (describir a continuación): **[...]**
* [ ] Uso de software libre para el desarrollo de las actividades del congreso (videoconferencia, chats, ... Indicar a continuación): **[...]**
* [ ] Ideas para incentivad la socialización (eventos sociales para el intercambio de ideas, discusión... Indicar a continuación): **[...]**

## Aspectos logísticos

* Relación con otras comunidades tecnológicas y/o divulgativas que pudieran participar: **[...]**
* Como se gestionaran las solicitudes de ponencias y las inscripciones de los asistentes: **[...]**
* [ ] Apoyo de alguna institución (indicar cual): **[...]**
* [ ] Nos comprometemos a hacer un informe de valoración post-evento, que además de servir para conocer la evolución del mismo, pueda ayudar a futuros equipos organizadores.

En el caso de evento presencial:

* Espacios disponibles en la propia sede para las actividades (número de salas disponibles, capacidad de la mismas, salón para actos de apertura y clausura...): **[...]**
* [ ] Los espacios son accesibles
* [ ] Existe facilidad para llegar con transporte público a la sede del congreso
* Medios de transporte disponibles para llegar al lugar de la sede y facilidad para llegar a la propia sede (carretera, autobús, tren...): **[...]**
* Relación con otras comunidades tecnológicas y/o divulgativas que pudieran participar: **[...]**
* Como se gestionaran las solicitudes de ponencias y las inscripciones de los asistentes: **[...]**
* Posible conciliación de familias asistentes al congreso (talleres de robótica, actividades lúdicas al aire libre...): **[...]**
* [ ] Apoyo de alguna institución (indicar cual): **[...]**
* [ ] Planes para actividades comunitarias fuera del horario del congreso (comida/cena de la comunidad, ruta turística/culturar, salida nocturna...): **[...]**
* [ ] Nos comprometemos a hacer un informe de valoración post-evento, que además de servir para conocer la evolución del mismo, pueda ayudar a futuros equipos organizadores.

En el caso de evento virtual:

* Espacios virtuales disponibles para las actividades (número máximo de salas disponibles, capacidad de la mismas, posibilidad de grabación): **[...]**
* [ ] Las charlas, salas y actos plenarios pueden ser grabados
* [ ] El software es accesible
* [ ] Planes para actividades comunitarias fuera del horario del congreso (reuniones virtuales): **[...]**


## Información de interés sobre el lugar de la sede (eventos presenciales)

* **[...]**

## Información extra que se considere de utilidad

* **[...]**

## Condiciones
* [ ] Aceptamos seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
