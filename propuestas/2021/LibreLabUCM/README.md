
# Propuesta para la Convocatoria de Sede esLibre 2021 de **LibreLabUCM**

## Equipo organizador

El equipo organizador de esta propuesta estará compuesto por (añadir tantas personas como sean necesarias):
* **Rafael Mateus Marques**
* **Cristóbal Saraiba Torres**
* **Pedro Javier Fernández**
* **Luis Solis Redondo**

### Experiencia y motivación

* Nuestra **asociación LibreLabUCM** se dedica a la difusión del software y conocimiento libre mediante la **realización de charlas/talleres/eventos** en diferentes lugares como facultades, institutos y también en diversos congresos.

* Nuestra experiencia es amplia, abarcando la organización de charlas y talleres, tanto en formato presencial como online, entre ellas destacar **eventos** como:
    *  [BufferHack 2019](https://bufferhack.librelabucm.org/).
    *  Semana del Software Libre.
    *  [HacktoberDay 2020](https://hacktoberday.librelabucm.org/) y también en la colaboración en eventos como esLibre, entre otros.

* Entre nuestras contribuciones al software libre se encuentran instancias de multiples **servicios libres** tales como plataformas de streaming como:
    *  Peertube
    *  Redes federadas (Mastodon)
    *  Aplicaciones para multiconferencias (Mumble).
* Ademas de estos servicios tambien hosteamos varios **mirrors oficiales** de SO entre ellos:
    *  Trisquel
    *  F-Droid
    *  Debian
    *  ArchLinux
    *  CentOs

Puedes acceder a una lista actualizada con todos nuestros mirrors y servicios [aquí](https://web.librelabucm.org/index.php/nuestros-proyectos/).

## Formato del congreso

Nuestra propuesta está basada en el formato de esLibre 2020 y esLibre 2019 junto a algunas modificaciones:
* El evento se realizaría de manera virtual y proponemos los siguientes tracks:

  * **Tracks Principal**
    * En este track se realizarán tanto el acto de bienvenida junto al acto de despedida y algunas sesiones especiales.
  * **Tracks Talleres**:
    * *Talleres I*: en este track estarán todos aquellos talleres que tengan una duración aproximada de 45 minutos.
    * *Talleres II*: en este track estarán todos aquellos talleres que tengan una duración aproximada de 2h.
  * **Track Salas** (ocurrirán de manera simultánea )
    * *Sala I*: en este track estarán todas aquellas ponencias que tengan una duración aproximada de 45 minutos.
    * *Sala II*: en este track estarán todas aquellas ponencias que tengan una duración aproximada de 45 minutos.
  * **Track "Devrooms"**
    * Existirán pequeños mini-tracks organizados por grupos com un máximo de 10 personas que se producirán de forma simultánea.
  * **Track "Mesas de Comunidades"**
    * Este track estará dedicado exclusivamente a proyectos de comunidades o mesas redondas.

## Posibles fechas

* Fechas estimadas para la realización del congreso:
    * Proponemos que se realice en el segundo **fin de semana de Mayo**.
* [X] Posibilidad de que el evento se desarrolle en fin de semana (total o parcialmente).
    * **Total**.
* [X] Nos comprometemos en la medida de lo posible a evitar que la fecha final del congreso choque con la de otro congreso de temática similar.

## Presupuesto económico

La asistencia al evento será **gratuita**.

* La asistencia al evento será gratuita.
* Posible financiación:
    * [X] Empresas privadas (nos comprometemos a que no sean por parte de compañías cuya actividad pudiera ser contraria a la filosofía del evento).
    * [X] Presupuestos/subvenciones de instituciones públicas.
* Posibles beneficios para el congreso en los que se invertiría el patrocinio:
    * Sorteo de diferentes dispositivos relacionados con el software libre.
    * Algún objeto con el logotipo de esLibre como agradecimiento para ponentes (tazas, camisetas o similares).

## Facilidades disponibles

Para la realización del evento utilizaremos las siguientes plataformas:

- Para la transmisión de los talleres utilizaremos **dos instancias** propias de **Jitsi** en diferentes redes y en diferentes dispositivos.
- Para las salas utilizaremos una plataforma que estamos desarrollando para eventos en la cual:
    - Todos los asistentes podrán realizar preguntas/dudas mediante **un chat** que se encontrará disponible en una plataforma que estamos desarrollando.
    - (*Opcional*) **Nuestra recomendación**: Las preguntas se leerán 10 minutos antes de finalizar la charla y finalice la ponencia, todas las preguntas/respuestas se volcarán a un Etherpad.
    - En cada ponencia existirán dos roles:
        -  **Moderador** en cada ponencia que se encargará de gestionar el chat de preguntas.
        - **Streamer**: se encargará de retransmitir dicha sala de Jitsi hacia la plataforma, evitando así una aglomeración dentro de la sala del ponente.


## Aspectos logísticos

* Relación con otras comunidades tecnológicas y/o divulgativas que pudieran participar:
    * HackMadrid%27
    * BitUp Alicante
    * Core UPM
    * LibreLabGRX
    * Gul UC3M

* [X] Nos comprometemos a hacer un informe de valoración post-evento, que además de servir para conocer la evolución del mismo, pueda ayudar a futuros equipos organizadores.

En el caso de evento virtual:

* Disponemos de varios servidores que actualmente soportan sin problema varios de los servicios que ofrecemos y que en caso de celebrar el evento desviaríamos temporalmente parte de esa capacidad a hostear las distintas salas del evento.

* [X] Las charlas, salas y actos plenarios pueden ser grabados.
* [X] El software es accesible.
* [X] Planes para actividades comunitarias fuera del horario del congreso (reuniones virtuales): *
    - Existirán **salas virtuales** (Hub Firefox).
    - Además, tenemos pensado organizar algunos **torneos de juegos libres** para fomentar así la participación en el evento.

## Presentación de candidaturas
Para la presentación de candidaturas hemos decidido mantener el formato del año pasado que funciona de la siguiente manera: [Presentación de Candidaturas](https://gitlab.com/eslibre/coord/-/tree/master/propuestas/2020#presentaci%C3%B3n-de-candidaturas).

## Condiciones
* [X] Aceptamos seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
