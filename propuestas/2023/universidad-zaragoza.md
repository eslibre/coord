---
layout: 2023/post
section: proposals
category: venues
title: Universidad de Zaragoza
---

## NOTA: Sede retirada por decisión de la organización ante la imposibilidad de hacerse cargo de la realización la entidad anfitriona proponente en las condiciones originalmente propuestas.

### Finalmente las comunidades anfitrionas pasaron a ser Vitalinux y migasfree, con la colaboración de Etopia como centro en el que realizarse las actividades: [https://eslib.re/2023/info-general/].

### Este archivo se mantiene simplemente por razones documentales.

## Equipo organizador local:

*Nombre del equipo*: Universidad de Zaragoza

Angel Bailo García responsable de la Oficina, lleva años en la informática trabajando en las áreas de Atención a Usuarios y Sistemas. Es el primer congreso que organiza pero tiene mucha ilusión.
Sergio Solé Rodríguez becario en la Oficina. Estudiante de Mecatrónica, le encanta el Software Libre.
Esperamos contar con voluntarios, entusiastas del software libre provenientes de comunidades locales y del propio Servicio de Informática de la Universidad.

## Formato del congreso:

Nuestra intención es que el formato sea mixto gracias a la experiencia que hemos adquirido durante la pandemia. Queremos volver a la presencialidad pero también queremos que los viajes no sean un problema y que todo el que quiera pueda asistir aunque sea por videoconferencia. Para facilitarlo, nuestras aulas y salones están dotados de webcam que se usan habitualmente para retransmitir las clases y los actos que lo requieren. Utilizando nuestro propio BBB retransmitiremos todas las sesiones

## Acciones para mejorar la diversidad del congreso:

Queremos apoyar la diversidad y, sobre todo la igualdad, en las TIC. Para ello queremos incentivar las ponencias femeninas y las de grupos minoritarios.

## Posibles fechas:

Todavía no tenemos fechas concretas pero como hasta ahora serían un viernes y un sábado en el mes de mayo y puede que como continuación del congreso RedIris que también se va a celebrar en Zaragoza.

## Aspectos económicos:

El mayor problema de la organización de un Congreso es el espacio y ya lo tenemos apalabrado con la Facultad de Educación. Nos han dado todas las facilidades y apoyo. En esta facultad disponemos de un amplio salón de actos y varias aulas. Toda la facultad cuenta con accesos adaptados y ascensores. Disponemos de zona de avituallamiento con mesas, fuente de agua fría y microondas.
Se buscarán patrocinadores con la intención de sufragar gastos de ponentes y poder dar algo de merchandising.

## Facilidades disponibles:

Dentro del Campus Universitario podemos encontrar varias cafeterías que sirven bebidas y comidas a precios fijos. Además, estamos en una zona universitaria rodeados de infinidad de bares y restaurantes para todos los gustos.
Respecto del alojamiento queremos buscar precios especiales en hoteles cercanos o en Colegios Mayores (aunque esto es muy difícil porque estamos en período lectivo)

## Planificación y uso del espacio físico:

El congreso se realizaría en la Facultad de Educación, sita en el Campus de San Francisco. Este campus está muy céntrico y bien comunicado con toda la ciudad con bus y tranvía. Además hay una gran red de carriles-bici.
Zaragoza es famosa por sus buenas comunicaciones y a ella se puede acceder muy fácilmente por carretera, tren y avión.

## Comentarios:

Queremos organizar este Congreso para darle un empujón a la presencia del software libre en la sociedad en general y, más concretamente, en las instituciones públicas aragonesas.
