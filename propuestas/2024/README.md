# Propuesta de sede esLibre 2024

Repositorio con todas las propuestas para ser grupo o comunidad anfitriona local de la organización de esLibre 2024.

Pues encontrar toda la información al respecto en <https://eslib.re/2024/propuesta-sede>.

Para cualquier duda, cuestión o sugerencias podéis contactarnos por correo o cualquier de nuestras redes:
- Correo electrónico: [hola@eslib.re](mailto:hola@eslib.re)
- Matrix: [#esLibre:matrix.org](https://matrix.to/#/#esLibre:http://matrix.org)
- Telegram: [@esLibre](https://t.me/esLibre)
- Mastodon: [@eslibre@hostux.social](https://hostux.social/@eslibre)
- Twitter: [@esLibre_](https://twitter.com/esLibre_)
