---
layout: 2024/post
section: proposals
category: venues
title: GNU/Linux Valencia
---

*Nombre del equipo*: GNU/Linux València

## Equipo organizador local:

- Julián Moyano  
- Salvador Mataix  
- Alejandro López  
- David Marzal  
- Baltasar Ortega  
- Tafol Nebot

## Formato del congreso:

- Conferencias de asistencia presencial y online, contenidas en dos días.  
- Install Party....  
- Exposiciones propias de Las Naves Centro de innovación social y urbana.  
- Actos de confraternización: comida popular

## Acciones para mejorar la diversidad del congreso:

Invitación a asociaciones comprometidas socialmente e inclusivas. Grupos feministas, asociaciones veganas, de la ciudad de Valencia, comunidad hacker.... 

## Posibles fechas:

Fechas previstas: 17-18 o 24-25 de mayo de 2024 (viernes y sábado). También podríamos ver la posibilidad de realizar tarde de Viernes, Sábado completo y Domingo mañana. Depende de las posibilidades de cesión de locales.

## Aspectos económicos:

- Los locales estarán proporcionados por el Ayto. de Valencia por medio de Las Naves Centro de innovación social y urbana.
- La cesión de espacios necesarios para el evento y medios técnicos (apertura en horario fin de semana para 1 técnico audiovisual, personal de recepción y limpieza). 
- Comunicación por canales de difusión de Las Naves.
- La aportación económica a determinar

## Facilidades disponibles:

- Se facilitara lugares y direcciones para pernoctar, comer y visitas de interés.
- Hay locales muy próximos con buena relación calidad/precio.
- Se facilitará información para realizar recorridos por la ciudad con contenido cultural, histórico y tecnológico.

## Planificación y uso del espacio físico:

- La Mutant preferiblemente por aforo para inauguración/cierre y espacio principal (220 personas) 
- Sala Polivalent (80-100)
- Sala Factoría (48)
- Sala Visual (30)
- Patios para caterings o alguna actividad extra, o inauguración/cierre para el aforo completo (capacidad para 185 y 148 personas)
- Pasarelas para actividad de cuidado infantil y salas extra para ponencias pensadas para 20-30 personas

## Comentarios:

Entendemos que aunque toda la información que incorporamos en este formulario es correcta, puede estar sujeta  a modificaciones que persigan mejorar el desarrollo del evento.