# Propuestas ser la sede de esLibre

 * **2019**:
     * [Club de Cacharreo](2019/ClubCacharreo/README.md)
     * [LibreLabGRX](2019/LibreLabGRX/README.md) (_presencial/seleccionada_)


 * **2020**:
     * [Oficina de Conocimiento y Cultura Libres de la Universidad Rey Juan Carlos](2020/OfiLibreURJC/README.md) (_virtual_)


 * **2021**:
     * [LibreLabUCM](2021/LibreLabUCM/README.md) (_virtual_)


 * **2022**:
     * [GALPon/AGASOL](2022/galpon-agasol.md) (_mixta_)


 * **2023**:
     * [Vitalinux/migasfree](2023/universidad-zaragoza.md) (_mixta_)

 * **2024**:
     * [GNU/Linux València](2024/gnu-linux-valencia.md) (_mixta_)