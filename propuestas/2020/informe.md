# Informe esLibre 2020

## Infraestructura

La celebración de esLibre 2020, que inicialmente iba a tener lugar de forma presencial en la Universidad Rey Juan Carlos, finalmente se celebró de forma virtual (online) con charlas en directo. 
Para ello, gracias a la colaboración de Antonio Gutiérrez, de los servicios informáticos de la ETSIT, se montó una infraestructura software, utilizando exclusivamente software libre, para celebrar el congreso de forma virtual.
Concretamente se utilizaron los siguientes servicios:

* Un servidor [BigBlueButton](https://bigbluebutton.org/) para las charlas y salas donde poder presentar mediante videoconferencia y grabar las charlas
* Un servidor [RocketChat](https://rocket.chat/) para las conversaciones alrededor de las charlas y donde poder crear canales temáticos si fuera necesario
* Un espacio virtual en [MozillaHubs](https://hubs.mozilla.com/) donde el viernes por la tarde pudimos conversar e intercambiar impresiones sobre el software y la cultura libre

La instalación de ambos servicios ser realizó en un servidor proporcionado por el servicio TIC de la URJC. El servidor disponía de 128Gb de RAM, 500Gb de disco y 24 CPUs, aunque no se llegó a utilizar más que el 10% de toda esta capacidad.

## Asistentes

En total, a lo largo de los dos días que duró el evento hubo alrededor de 200 asistentes. En determinados momentos del viernes 19 de septiembre, llegó a haber más de 100 personas conectadas a BBB a la vez.
La infraestructura montada funcionó perfectamente soportando bien la carga. 


El viernes por la mañana hubo 5 Salas Activas paralelamente con unos 200-270 usuarios de BBB activos a la vez. Por la tarde unas 3 Salas Activas con 100 - 150 usuarios.

El sábado hubo 4 salas incluyendo la plenaria. Durante la mañana hubo entre 100 y 150 usuarios. Por la tarde, 3 salas activas, unos 75 - 100 usuarios.

## Eventos sociales

Para facilitar la interacción entre los asistentes se llevaron a cabo dos acciones:

* Proporcionar un servidor de chat (Rocket Chat) para favorecer el intercambio de ideas
* Proporcionar un evento social en Mozilla Hubs, planificado el viernes por la tarde, donde se dispusieron diferentes salas a las cuales los asistentes se podían unir. En general, este entorno virtual causó muy buena impresión, aunque en ocasiones el rendimiento con tanta gente dentro disminuía

## Alcance

TBD



