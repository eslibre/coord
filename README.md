# Coordinación

Este es el proyecto para la coordinación **esLibre**, el congreso sobre cultura libre y tecnologías libres. Desde aquí se gestionan las propuestas de sedes para las diferentes ediciones. Puedes encontrar la información sobre las ediciones anteriores.

Se libre de participar en cualquier parte, tanto en los issues que se abran para discutir y coordinar en el [grupo abierto de Telegram](https://t.me/esLibre), que también está enlazado con la [sala de Matrix](https://matrix.to/#/#esLibre:matrix.org).

También puedes escribirnos por correo electrónico a [hola@eslib.re](mailto:hola@eslib.re).
